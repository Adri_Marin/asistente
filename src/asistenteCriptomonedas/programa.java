package asistenteCriptomonedas;
import java.util.Scanner;
/**
 * 
 * @author marin
 * @since 05/03/2021
 * El programa trata de un asistente de criptomonedas que ayuda al trader a hacer los calculos necesarios para 
 * hacer m�s precisas sus operaciones. 
 *
 */
public class programa {
/**
 * La clase main contiene la base del programa, el cual pregunta los datos necesarios al usuario para hacer los calculos.
 * Tambi�n llama a los dem�s m�todos que se tienen que ejecutar. 
 * @param args main
 */
	public static void main(String args[]) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce el n�mero de veces que quieres calcular:");
		int x= 0;
		int casos = sc.nextInt();
		
		do {
		x=x+1;
			
		System.out.println("Cual es el precio de entrada al mercado?");
		float entrada = sc.nextInt();
		
		System.out.println("Cual es el precio de salida del mercado?");
		float salida = sc.nextInt();
		
		System.out.println("Cual es la cantidad de la operaci�n?");
		float cantidad = sc.nextInt();

		
		System.out.println("Cual es el porcentaje deseado a obtener?");
		float porcentaje = sc.nextInt();
		
		System.out.println("Cual es el n�mero de apalancamiento que usas?");
		float numero = sc.nextInt();
		
		System.out.println("Cual es el balance de la billetera?");
		float balance = sc.nextInt();
		
		programa.resultado(entrada,salida,cantidad);
		programa.objetivo(entrada,cantidad,porcentaje);
		programa.liquidacion(entrada,cantidad,balance,numero);
		
		}while(x<casos);
		
		
	}
	
/**
 * Entran tres parametros, entrada, salida, cantidad y hace el calculo necesario para obtener el resultado de la operacion, 
 * sea positivo o negativo. 
 * @param entrada precio de entrada	
 * @param salida precio de salida 	
 * @param cantidad cantidad de la operacion 
 * @return resultado resultado total de la operacion
 */
		
	public static float resultado(float entrada, float salida, float cantidad) {
		
		float resultado = salida * cantidad / entrada;
		System.out.println("Ahora tu cantidad es:" + resultado);
		return resultado;
		
		
	}

/**
 * Entran tres parametros, entrada, cantidad, porcentaje y hace el calculo necesario para obtener el precio objetivo para
 * obtener el porcentaje introducido. 
 * @param entrada precio de entrada
 * @param cantidad cantidad de la operacion
 * @param porcentaje porcentaje deseado a obtener
 * @return objetivo precio a alcanzar
 */
	public static float objetivo(float entrada, float cantidad, float porcentaje) {
		
		float operacion1 = porcentaje/100;
		float operacion2 = operacion1 +1;
		float objetivo = operacion2 * entrada; 
		System.out.println("El precio objetivo es:" + objetivo);
		return objetivo;
		
		
	}
	/**
	 * Entran 4 parametros, entrada,cantidad, balance, numero y hace el calculo necesario para obtener el precio en el cual 
	 * la billetera seria liquidada por completo.
	 * @param entrada precio de entrada
	 * @param cantidad cantidad de la operacion	
	 * @param balance balance total de la billetera
	 * @param numero numero de apalanacamiento
	 * @return liquidacion precio donde se liquida la billetera
	 */
public static float liquidacion(float entrada, float cantidad,float balance,float numero) {
		
		float porcentajebilletera = cantidad *100 / balance;
		float operacion1 = porcentajebilletera * numero;
		float operacion2 = 100 / operacion1; 
		float operacion3 = 1 - operacion2;
		float liquidacion = entrada * operacion3;
		System.out.println("El precio de liquidaci�n es:" + liquidacion);
		return liquidacion;

	}
}

	






