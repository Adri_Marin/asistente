package asistenteCriptomonedas;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class programaTest {
	
	@Test
	public void tesResultado() {
		float resultado = programa.resultado(100,200,10);
		float esperado = 20;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testObjetivo() {
		float resultado = programa.objetivo(100,10,50);
		float esperado = 150;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testLiquidacion() {
		float resultado = programa.liquidacion(100,10,100,2);
		float esperado = -400;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void tesResultado1() {
		float resultado = programa.resultado(500,3200,37);
		float esperado = (float) 236.8;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testObjetivo1() {
		float resultado = programa.objetivo(500,37,300);
		float esperado = 2000;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testLiquidacion1() {
		float resultado = programa.liquidacion(500,37,59,10);
		float esperado = (float) 420.27026;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void tesResultado2() {
		float resultado = programa.resultado(1234,9271,934);
		float esperado = (float) 7017.1104;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testObjetivo2() {
		float resultado = programa.objetivo(1234,934,1300);
		float esperado = 17276;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testLiquidacion2() {
		float resultado = programa.liquidacion(1234,934,1300,125);
		float esperado = (float)1220.2595;
		assertEquals(esperado, resultado);
	}

}
